# AlexLab Cloud's Custom Node.js Tool Configurations

Reusable configurations for ESLint, Prettier, and TypeScript.

## Packages

### [@alexlab-cloud/tsconfig](./packages/tsconfig/)

TSConfig files for various flavors of Node.js projects, including a robust base configuration.

### [@alexlab-cloud/eslint-config](./packages/eslint-config/)

A standardized ESLint configuration to use as the base in all AlexLab Cloud Node.js projects.

### [@alexlab-cloud/prettier-config](./packages/prettier-config/)

A standardized Prettier configuration to use as the base in all AlexLab Cloud Node.js projects.

<hr>

<sub>Emoji used for repository logo designed by <a href="https://openmoji.org/">OpenMoji</a> – the open-source emoji and icon project. License: CC BY-SA 4.0</sub>

<hr>
