# AlexLab Cloud's Custom TSConfig Configurations

`@alexlab-cloud/tsconfig`

Reusable configurations for TypeScript's [TSConfig](https://www.typescriptlang.org/tsconfig) format.

## Installing

```bash
pnpm add -D @alexlab-cloud/tsconfig-custom
```

## Using

[TSConfig's `"extends"` key](https://www.typescriptlang.org/tsconfig#extends) can be used to include these custom
configurations. Specify the config flavor with its path.

```json
{
    "extends": "@alexlab-cloud/tsconfig-custom/flavors/base.json",
    "compilerOptions": {
        "outDir": "dist"
    },
    "include": ["src/**/*.ts"],
    "exclude": ["dist", "node_modules"]
}
```
