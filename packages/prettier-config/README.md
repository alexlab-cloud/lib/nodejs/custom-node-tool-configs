# AlexLab Cloud's Custom Prettier Configurations

`@alexlab-cloud/prettier-config`

Reusable configurations for [Prettier](https://prettier.io/), as documented
[here](https://prettier.io/docs/en/configuration.html#sharing-configurations).

## Installing

```bash
pnpm add -D @alexlab-cloud/prettier-config
```

## Using

The simplest method for using this package is adding it to a project's `package.json`:

```json
{
    "scripts": {
        "fmt": "prettier --check ."
    },
    "prettier": "@alexlab-cloud/prettier-config/node-base"
}
```

Ensure your editor's Prettier integration is configured to take full advantage of the formatting capabilities.
