/** @type { import('prettier').Config } */
export default {
  tabWidth: 4,
  useTabs: false,
  trailingComma: 'es5',
  semi: true,
  singleQuote: true,
  quoteProps: 'consistent',
  bracketSpacing: true,
  arrowParens: 'always',
  printWidth: 100,
};
