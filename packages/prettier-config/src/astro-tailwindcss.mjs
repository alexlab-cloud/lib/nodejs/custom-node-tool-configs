import basePrettierConfig from './base.mjs'

/** @type { import('prettier').Config } */
export default {
  ...basePrettierConfig,
  tailwindConfig: './tailwind.config.mjs',
  plugins: [
    'prettier-plugin-astro',
    'prettier-plugin-tailwindcss',  // ⚠️ Order is very important: place Tailwind prettier plugin at the end
  ],
  overrides: [
    {
      files: '*.astro',
      options: {
        parser: 'astro',
      },
    },
  ],
};
