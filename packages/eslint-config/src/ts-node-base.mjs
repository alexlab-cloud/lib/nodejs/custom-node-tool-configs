import { configure, presets } from 'eslint-kit'

/** @type { import('eslint').Linter.Config } */
export default configure({
  mode: 'only-errors',
  presets: [
    presets.imports(),
    presets.typescript(),
    presets.tsdoc(),
    presets.prettier(),
    presets.node(),
  ],
  extend: {
    rules: {
    }
  }
})
