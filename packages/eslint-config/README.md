# AlexLab Cloud's Custom ESLint Configurations

`@alexlab-cloud/eslint-custom`

Reusable configurations for [ESLint](https://eslint.org/).

## Installing

```bash
pnpm add -D @alexlab-cloud/eslint-config
```

## Using

To use this package, specify it in the `extends` array of any Node project's `.eslintrc.cjs`:

```javascript
module.exports = {
    extends: ["@alexlab-cloud/eslint-custom"], /* Can also extend additional configurations */
    ignorePatterns: ["dist/*", ".eslintrc.js"], /* Flexible per project */
};
```
